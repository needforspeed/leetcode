/*
 Problem:    House Robber
 Difficulty: Easy
 Source:     https://leetcode.com/problems/house-robber/
 Notes:
 You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

 Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.

 Credits:
 Special thanks to @ifanchu for adding this problem and creating all test cases. Also thanks to @ts for adding additional test cases.

 Solution: 
 */

class Solution {
public:
    int rob(vector<int>& nums) {
        int include_prev = 0, exclude_prev = 0;
        for(auto n : nums) {
            int tmp = include_prev;
            
            // for the new round: exclude_prev + current
            include_prev = n + exclude_prev;
            
            // for the new round: include_prev, can't rob current
            exclude_prev = max(tmp, exclude_prev);
        }
        return max(include_prev, exclude_prev);
    }
};

