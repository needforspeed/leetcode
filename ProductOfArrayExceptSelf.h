/*
 Problem:    Product of Array Except Self
 Difficulty: Medium
 Source:     https://leetcode.com/problems/product-of-array-except-self/
 Notes:
 Given an array of n integers where n > 1, nums, return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].

 Solve it without division and in O(n).

 For example, given [1,2,3,4], return [24,12,8,6].

 Follow up:
 Could you solve it with constant space complexity? (Note: The output array does not count as extra space for the purpose of space complexity analysis.)

 Solution:
A[0], A[1], A[2], A[i],..., A[n];
process in two passes for a given position i:
 * first pass:
     results[i] = A[0] * A[1] * A[2] * ... * A[i - 1];
 * second pass:
     results[i] *= A[i + 1] * A[i + 2] * ... * A[n];
*/
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int n = nums.size();
        vector<int> results(n, 1);
        
        for(int i = 1; i < n; i++) {
            results[i] = results[i - 1] * nums[i - 1];
        }
        
        int tmp = 1;
        
        for(int i = n - 1; i >= 0; i--) {
            results[i] *= tmp;
            tmp *= nums[i];
        }
        
        return results;
    }
};

