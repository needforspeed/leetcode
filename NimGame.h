/*
 Problem: Nim Game
 Difficulty: Easy
 Source:     https://leetcode.com/problems/nim-game/
 Notes:
 You are playing the following Nim Game with your friend: There is a heap of stones on the table, each time one of you take turns to remove 1 to 3 stones. The one who removes the last stone will be the winner. You will take the first turn to remove the stones.

 Both of you are very clever and have optimal strategies for the game. Write a function to determine whether you can win the game given the number of stones in the heap.

 For example, if there are 4 stones in the heap, then you will never win the game: no matter 1, 2, or 3 stones you remove, the last stone will always be removed by your friend.

 Hint:

    If there are 5 stones in the heap, could you figure out a way to remove the stones such that you will always be the winner?

 Credits:
 Special thanks to @jianchao.li.fighter for adding this problem and creating all test cases.

 Solution:
1 -> true
2 -> true
3 -> true

4 -> false

5 - 1 -> true
6 - 2 -> true
7 - 3 -> true

8 - 1 => 7 -> false
8 - 2 => 6 -> false
8 - 3 => 5 -> false
8 -> false

9 - 1 => 8 -> true
9 - 2 => 7 -> false
9 - 3 => 6 -> false
9 -> true

10 - 1 => 9 -> false
10 - 2 => 8 -> true
10 - 3 => 7 -> false
10 -> true

11 - 1 => 10 -> false
11 - 2 => 9  -> false
11 - 3 => 8  -> true
11 -> true
*/

class Solution {
public:
    bool canWinNim(int n) {
        return (n % 4);
    }
};

