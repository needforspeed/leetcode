/*
 Word Pattern

 Given a pattern and a string str, find if str follows the same pattern.

 Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.

 Examples:

    pattern = "abba", str = "dog cat cat dog" should return true.
    pattern = "abba", str = "dog cat cat fish" should return false.
    pattern = "aaaa", str = "dog cat cat dog" should return false.
    pattern = "abba", str = "dog dog dog dog" should return false.

 Notes:
 You may assume pattern contains only lowercase letters, and str contains lowercase letters separated by a single space.

 Credits:
 Special thanks to @minglotus6 for adding this problem and creating all test cases.

*/

/*
use istringstream to parse the words in the str
*/
class Solution {
public:
    bool wordPattern(string pattern, string str) {
        map<char, int> char_position;
        map<string, int> string_position;
        istringstream iss(str);
        int i = 0, n = pattern.length();
        for(string word ; iss >> word; ++i) {
            if(i==n || char_position[pattern[i]] != string_position[word]) {
                return false;
            }
            // using i + 1,
            // because if the key => value mapping doesn't exist, then below holds
            // map[idx] == 0;
            char_position[pattern[i]] = string_position[word] = i+1;
        }
        // if pattern has same length of words of str, then true, else false.
        return i == n;
    }
};

